require('dotenv').config({ path: '.env' });

<<<<<<< Updated upstream
console.log(
  process.env.DATABASE,
  process.env.USER_NAME,
  process.env.PASSWORD,
  process.env.HOST,
  process.env.PORT
);
=======
const { DB_HOST, DB_PORT, DB_NAME, DB_USERNAME, DB_PASSWORD } = process.env;
>>>>>>> Stashed changes

const standardConfig = {
  client: 'pg',
  connection: {
<<<<<<< Updated upstream
    database: process.env.DATABASE,
    user: process.env.USER_NAME,
    password: process.env.PASSWORD,
    host: process.env.HOST,
    port: process.env.PORT,
    insecureAuth: true,
=======
    host: DB_HOST,
    port: parseInt(DB_PORT, 10) || 27017,
    user: DB_USERNAME,
    password: DB_PASSWORD,
    database: DB_NAME,
  },
  migrations: {
    tableName: 'migrations',
    extension: 'js',
>>>>>>> Stashed changes
  },
};

module.exports = {
  development: {
    ...standardConfig,
  },

  staging: {
    ...standardConfig,
  },

  production: {
    ...standardConfig,
  },
};
