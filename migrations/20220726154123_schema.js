exports.up = async function (knex) {
  return knex.schema.createSchema('urls');
};

exports.down = async function (knex) {
  return knex.schema.dropSchema('urls');
};
