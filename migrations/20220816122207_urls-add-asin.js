exports.up = async function (knex) {
  const tables = await knex.select('*').from('information_schema.tables').where('table_schema', 'urls').returning('*');

  return tables.map(async value => {
    await knex.schema
      .withSchema('urls')
      .hasColumn(value.table_name, 'is_has_asin')
      .then(async exists => {
        if (!exists) {
          await knex.schema.withSchema('urls').alterTable(value.table_name, table => {
            table.integer('is_has_asin');
          });
        }
      });
  });
};

exports.down = async function (knex) {
  const tables = await knex.select('*').from('information_schema.tables').where('table_schema', 'urls').returning('*');

  return tables.map(async value => {
    await knex.schema.withSchema('urls').alterTable(value.table_name, table => {
      table.dropColumn('is_has_asin');
    });
  });
};
