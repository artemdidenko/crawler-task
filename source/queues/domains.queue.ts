import Queue from 'bull';
import { generateQueue } from './default.queue';

let _queue = null;

export const getDomainsQueue = (): Queue.Queue => {
  if (!_queue) {
    _queue = generateQueue('domains', 1);
  }
  return _queue;
};
