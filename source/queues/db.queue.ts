import Queue from 'bull';
import { generateQueue } from './default.queue';

let _queue = null;

export const getDBQueue = (): Queue.Queue => {
  if (!_queue) {
    _queue = generateQueue('db', 2);
  }
  return _queue;
};
