import Queue from 'bull';
import { getRedisConfig } from '../common/db/redis';
import { defaultJobOptions, settings } from './queue.config';

export const generateQueue = (name: string, db: number) => {
  const queue = new Queue(name, {
    redis: getRedisConfig(),
    defaultJobOptions,
    settings,
  });
  return queue;
};
