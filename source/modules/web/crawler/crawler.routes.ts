import { Router } from 'express';
import { CrawlerController } from './crawler.controller';

export const createCrawlerRouter = () => {
  const router = Router();
  router.post('/publisher/:publisher_id/schedule', CrawlerController.requestCrawling);
  router.get('/publisher/:publisher_id/status', CrawlerController.getStatusByPublisher);
  router.get('/publisher/:publisher_id', CrawlerController.listCrawlingUrlsByPublisher);

  return router;
};
