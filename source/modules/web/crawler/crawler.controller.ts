import { ExpressRequest, ExpressResponse } from '../../../common/types';
import { parse } from 'tldts';
import { getDomainsQueue } from '../../../queues/domains.queue';
import { urlsRepository } from '../../../common/repositories/urls.repository';
import { IDomainDB } from '../../../common/db/interfaces/domain.interface';

const requestCrawling = async (req: ExpressRequest, res: ExpressResponse) => {
  const { hostname, domainWithoutSuffix } = parse(req.body.url);

  const isHasDomain: Array<IDomainDB> = await urlsRepository.isHasDomain('https://' + hostname);

  if (['active', 'pending'].includes(isHasDomain[0]?.status)) {
    return res.json({ domain: 'https://' + hostname, status: isHasDomain[0].status });
  }

  const getDomainDB: Array<IDomainDB> = await urlsRepository.upsertStatusDomain({
    domain: 'https://' + hostname,
    status: 'pending',
    publisher_id: req.params.publisher_id,
    updated_at: new Date().toISOString(),
  });

  const q = getDomainsQueue();

  if (!getDomainDB) throw new Error('unable to insert queue status');

  await q.add({
    domain: 'https://' + hostname,
    tablename: domainWithoutSuffix,
    id: getDomainDB[0].id,
  });

  res.json({
    domain: 'https://' + hostname,
    status: 'active',
  });
};

const getStatusByPublisher = async (req: ExpressRequest, res: ExpressResponse) => {
  const statusByPublisher: Array<IDomainDB> = await urlsRepository.getListDomainsByPublisher(req.params.publisher_id);

  res.json({
    status: statusByPublisher[0] ?? null,
  });
};

const listCrawlingUrlsByPublisher = async (req: ExpressRequest, res: ExpressResponse) => {
  const LIMIT: number = parseInt(req.query.limit?.toString()) || 100,
    OFFSET: number = parseInt(req.query.offset?.toString()) || 0,
    SEARCH: string = req.query.search?.toString() || '',
    WITH_ASINS: number = parseInt(req.query.with_asins?.toString()) || 0;

  const domainByPublisher: Array<IDomainDB> = await urlsRepository.getListDomainsByPublisher(req.params.publisher_id);

  if (!domainByPublisher[0]) return res.json({ total_count: 0, urls: [] });

  const { domainWithoutSuffix } = parse(domainByPublisher[0]?.domain);

  const { totalCount, urls } = await urlsRepository.getListUrlsByDomain(
    domainWithoutSuffix,
    LIMIT,
    OFFSET,
    SEARCH,
    WITH_ASINS
  );

  res.json({
    total_count: totalCount ?? 0,
    urls: urls ?? [],
  });
};

export const CrawlerController = {
  requestCrawling,
  getStatusByPublisher,
  listCrawlingUrlsByPublisher,
};
