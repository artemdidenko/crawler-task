import Queue, { DoneCallback } from 'bull';
import { urlsRedisClient } from '../../common/db/redis';
import { getLogger } from '../../common/logging';
import { crawlingJob } from '../../jobs/urls-job/urls-job.controller';
import { getDomainsQueue } from '../../queues/domains.queue';
import { urlsRepository } from '../../common/repositories/urls.repository';
import { getDBQueue } from '../../queues/db.queue';

export const buildWorker = async (): Promise<void> => {
  const domainsQueue = getDomainsQueue();
  const databaseQueue = getDBQueue();
  const log = getLogger();

  const queueSize = await domainsQueue.getJobCounts();
  log.info({ queueSize });

  domainsQueue
    .process(10, async (job: Queue.Job, done: DoneCallback) => {
      await urlsRepository.createTableWithURLs(job.data.tablename);
      await crawlingJob(job, done);

      await urlsRepository.putStatusDomain({ status: 'active', updated_at: new Date().toISOString() }, job.data.id);
      log.info(`Crawling ${job.data.domain} domain [started]`);
    })
    .catch(log.error);

  databaseQueue
    .process(10, async (job: Queue.Job, done: DoneCallback) => {
      await urlsRepository.insertUrl(job.data.arrDB, job.data.domain as string);
      done();
    })
    .catch(log.error);
};
