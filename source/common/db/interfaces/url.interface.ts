export interface IUrlDB {
  id: number;
  url: string;
  domain_id: number;
  is_has_asin: number;
}
