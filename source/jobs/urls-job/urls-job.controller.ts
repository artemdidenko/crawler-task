import Queue, { DoneCallback } from 'bull';
import { parse } from 'tldts';
import { anchorsOfCheerio, parseHref } from './urls-job.service';
import { urlsRedisClient } from '../../common/db/redis';
import { urlsRepository } from '../../common/repositories/urls.repository';
import { getLogger } from '../../common/logging';
import axios from 'axios';
import { destroyCrawlingQueue, getCrawlingQueue } from '../../queues/crawling.queue';
import { getDBQueue } from '../../queues/db.queue';

export const crawlingJob = async (job: Queue.Job, domainQueueDone: DoneCallback) => {
  const { domain, id: domainId } = job.data;
  const { domainWithoutSuffix, hostname } = parse(domain);

  const q = getCrawlingQueue(domain);
  const qDB = getDBQueue();
  const log = getLogger();
  let arrWithUrl = [];
  let flag: boolean = false;

  q.process(50, async (job: Queue.Job, done: DoneCallback) => {
    try {
      const { data: html } = await axios(job.data.url);

      const anchors = anchorsOfCheerio(html);

      await urlsRedisClient().zadd(domainWithoutSuffix as string, '0', job.data.url);

      Object.keys(anchors).forEach(async item => {
        if (anchors[item].type === 'tag') {
          const href: string = anchors[item].attribs.href?.trim();
          const parsedUrl: string = parseHref(href, domain);

          if (parsedUrl.match(/\/dp\/([A-Z0-9]{10})/g)) {
            flag = true;
          }

          const isExists: boolean =
            (await urlsRepository.isExistsUrl(parsedUrl, domainWithoutSuffix as string)) === null ? false : true;

          if (parsedUrl && parsedUrl.toString().includes(hostname) && !isExists) {
            q.add({
              url: encodeURI(parsedUrl),
            });
          }
        }
      });

      arrWithUrl.push({
        url: job.data.url,
        domain_id: domainId,
        is_has_asin: flag ? 1 : 0,
      });
      flag = false;

      if (arrWithUrl.length >= 500) {
        await qDB.add({ domain: domainWithoutSuffix, arrDB: arrWithUrl });
        arrWithUrl.length = 0;
      }

      done();
    } catch (err) {
      if (!err.response) log.error(err);
      done(err);
    }
  });

  q.add({ url: domain });

  const logsInternalURLId = setInterval(async () => {
    const queueSize = await q.getJobCounts();
    log.info({ queueSize, domain }, ` Crawling ${domain} domain [in progress]`);

    if (queueSize.active === 0 && queueSize.waiting === 0) {
      domainQueueDone();
      await urlsRepository.putStatusDomain(
        { status: 'finished', updated_at: new Date().toISOString(), last_crawled: new Date().toISOString() },
        domainId
      );
      destroyCrawlingQueue(domain);
      await urlsRedisClient().del(domainWithoutSuffix);
      log.info({ queueSize, domain }, `Crawling ${domain} domain [finished]`);
      clearInterval(logsInternalURLId);
    }
  }, 5000);
};
