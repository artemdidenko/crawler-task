import * as cheerio from 'cheerio';
import { getLogger } from '../../common/logging';
import { isWebUri } from 'valid-url';

export const anchorsOfCheerio = html => {
  const $ = cheerio.load(html);
  const anchors = $('a');

  return anchors;
};

const isURL = (href: string) => {
  const log = getLogger();

  if (isWebUri(href)) {
    try {
      const url = new URL(href);
      const newUrl: string = url.origin + url.pathname;

      return newUrl.replace(/(.+)(\/|#)$/, '$1');
    } catch (error) {
      log.error('error', error);
    }
  }

  return '';
};

export const parseHref = (href: string, domain: string): string => {
  const newUrl = isURL(href);

  return newUrl || isURL(`${domain}${href}`);
};
